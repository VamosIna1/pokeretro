package com.example.pokeretro.Network

import com.example.pokeretro.Model.ResPokeData
import retrofit2.Call
import retrofit2.http.GET

interface ServiceApi {
    @GET("pokemon")
    fun getData(): Call<ResPokeData>
}