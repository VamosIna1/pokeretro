package com.example.pokeretro.Presenter

import com.example.pokeretro.Model.ResPokeData
import com.example.pokeretro.Network.ClientApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainPresenterPoke(private  val mainPresenterPokeBindView: MainPresenterPokeBindView) {
    fun getDataApiPoke(){
        ClientApi.instance.getData().enqueue(object : Callback<ResPokeData> {
            override fun onFailure(call: Call<ResPokeData>, t: Throwable) {
                mainPresenterPokeBindView.onError(t.localizedMessage)
            }
            override fun onResponse(call: Call<ResPokeData>, response: Response<ResPokeData>) {
                if (response.isSuccessful){
                    mainPresenterPokeBindView.onSuccess(response.body())
                }else mainPresenterPokeBindView.onError("Data kosong")
            }


        })
    }
}