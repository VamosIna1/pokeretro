package com.example.pokeretro.Presenter

import com.example.pokeretro.Model.ResPokeData

interface MainPresenterPokeBindView {
    fun onSuccess(data: ResPokeData?)
    fun onError(msg: String)
}