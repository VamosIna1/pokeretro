package com.example.pokeretro.View.Main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokeretro.Model.ResultsItem
import com.example.pokeretro.R
import kotlinx.android.synthetic.main.item_list.view.*

class MainPokeAdapter(var listDataitemPoke :List<ResultsItem?>?,var context: Context) :RecyclerView.Adapter<MainPokeAdapter.ViewHolder>() {
    private lateinit var imagePokeDataItem :ArrayList<String?>

    inner class ViewHolder(ItemView : View) :RecyclerView.ViewHolder(ItemView){
        val imagePokeDataItem = itemView.image_poke
        val namaPoke = itemView.tv_name_poke
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPokeAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int =listDataitemPoke?.size!!

    override fun onBindViewHolder(holder: MainPokeAdapter.ViewHolder, position: Int) {
        var image = listDataitemPoke?.get(position)
        Glide.with(context).load(getImageUrl(image?.url!!)).into(holder.imagePokeDataItem)
        holder.namaPoke.text = listDataitemPoke?.get(position)?.name

    }
    fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }

}