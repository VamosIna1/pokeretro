package com.example.pokeretro.View.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pokeretro.Model.ResPokeData
import com.example.pokeretro.Presenter.MainPresenterPoke
import com.example.pokeretro.Presenter.MainPresenterPokeBindView
import com.example.pokeretro.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),MainPresenterPokeBindView {
    private lateinit var mainPokeAdapter: MainPokeAdapter
    private lateinit var mainPresenterPokemon: MainPresenterPoke

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupDataComponent()
    }

    private fun setupDataComponent(){
        mainPresenterPokemon = MainPresenterPoke(this@MainActivity)
        mainPresenterPokemon.getDataApiPoke()
    }


    override fun onSuccess(data: ResPokeData?) {
        Log.d("dataResult",data?.results.toString())
        mainPokeAdapter= MainPokeAdapter(data?.results,this@MainActivity)
        rv_main.apply {
            rv_main.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rv_main.setHasFixedSize(true)
            rv_main.adapter = mainPokeAdapter

        }
    }

    override fun onError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}
